var qoutes = [
    {
    name:"-Oscar Wilde",
    text:"“Be yourself; everyone else is already taken.”"
   },
   {
    name:"-Albert Einstein",
    text:"“Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.”"
   },
   {
    name:"-Frank Zappa",
    text:"“So many books, so little time.”"
   },
   {
    name:"-Dr. Seuss",
    text:"“You know you're in love when you can't fall asleep because reality is finally better than your dreams.”"
   },
   {
    name:"-Mae West",
    text:"“You only live once, but if you do it right, once is enough.”"
   },
   {
    name:"-Robert Frost",
    text:"“In three words I can sum up everything I've learned about life: it goes on.”"
   },
   {
    name:"-Mark Twain",
    text:"“If you tell the truth, you don't have to remember anything.”"
   }
]

function CreateQoutes(){

    for (var i =0 ; i<qoutes.length; i++ ){
        var randomnumber = Math.floor(Math.random() * qoutes.length)
        var authorQoutes= qoutes[randomnumber].name;
        var TextQoutes= qoutes[randomnumber].text;
    }
    document.getElementById("author").innerHTML=authorQoutes;
    document.getElementById("qouteText").innerHTML=TextQoutes;
}
